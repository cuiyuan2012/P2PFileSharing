package servlet;

import p2p.Peer;
import p2p.PeerImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by cuiyuan on 3/14/16.
 */
@WebListener
@WebServlet(name = "peer", urlPatterns = {"/file"})
public class PeerServlet extends HttpServlet implements ServletContextListener{

    //private PeerConfig config = PeerConfig.getInstance();
    private Peer peer = PeerImpl.getInstance();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        //get filename which is requested
        String filename = req.getParameter("filename");
        //write file to response
        try {
            long start = System.currentTimeMillis();
            peer.writeFileToResponse(filename, resp, getServletContext());

            System.out.println(String.format("File %s has sent to another peer in %s milliseconds",
                    filename, System.currentTimeMillis() - start));
        } catch (FileNotFoundException e) {
            try {
                resp.sendError(404, "File Not Found!");
            } catch (IOException e1) {
                //TODO: change to system.out at least, better to use a logger like log4j.
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //peer.registerInstance(config.getIndexingServerAddresses().get(0));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
