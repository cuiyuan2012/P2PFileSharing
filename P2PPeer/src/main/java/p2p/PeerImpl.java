package p2p;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.ContentResponseHandler;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import util.PeerConfig;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by cuiyuan on 3/14/16.
 */
public class PeerImpl implements Peer {


    private PeerConfig config = PeerConfig.getInstance();

    private static PeerImpl instance = new PeerImpl();

    public static PeerImpl getInstance() {
        return instance;
    }
    private PeerImpl(){};

    @Override
    public void registerInstance(String indexingServerAddress) {
        try {
            //send post request to "/register" at indexing server
            //https://hc.apache.org/httpcomponents-client-ga/quickstart.html
            String peer_addr = String.format("http://%s:%s/file", config.getIp_addr(), config.getPortNum());
            String files = "";
            for (String filename : getFileNameList()) {
                files += (filename + "|");
            }
            String requestUrl = null;
            if (!indexingServerAddress.endsWith("/register")) {
                requestUrl = indexingServerAddress + (indexingServerAddress.endsWith("/")?"register":"/register");
            }
            System.out.println("Sending post request to " + requestUrl);
            String responseStr = Request.Post(requestUrl)
                    .bodyForm(Form.form().add("peer_addr", peer_addr).add("files", files).build())
                    .execute().returnContent().asString();
            System.out.println(String.format("Got response '%s'", responseStr));
        } catch (IOException ex) {
            Logger.getLogger(PeerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public List<String> getFileNameList() {
        List<String> result = new ArrayList<>();
        try {
            //list all files in a directory specified by "sharedDirPath".
            //https://dzone.com/articles/java-example-list-all-files
            Files.list(Paths.get(config.getSharedDirPath())).forEach(file -> {
                result.add(file.toFile().getName());
            });

        } catch (IOException ex) {
            Logger.getLogger(PeerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public String searchFile(String fileName) {
        //send get request to "/search" at indexing server
        //https://hc.apache.org/httpcomponents-client-ga/quickstart.html
        final String[] result = new String[1];
        System.out.println("Searching through " + config.getIndexingServerAddresses().size() + " indexing servers.");
        config.getIndexingServerAddresses().stream().forEach(new Consumer<String>() {
            @Override
            public void accept(String indexingServer) {
                try {
                    long start = System.currentTimeMillis();
                    String requestURL = indexingServer + "/search?filename=" + fileName;
                    Response response = Request.Get(requestURL).execute();
                    HttpResponse httpResponse = response.returnResponse();
                    if (httpResponse.getStatusLine().getStatusCode() == HttpServletResponse.SC_OK) {
                        ContentResponseHandler contentResponseHandler = new ContentResponseHandler();
                        result[0] = contentResponseHandler.handleEntity(httpResponse.getEntity()).asString();
                    }
                    System.out.println(String.format("File %s found at peer '%s' in %s milliseconds",
                            fileName, result[0], System.currentTimeMillis() - start));
                } catch (IOException ex) {
                    Logger.getLogger(PeerImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        return result[0];
    }

    /**
     * @param src      e.g. : http://host:port/file
     * @param fileName
     */
    @Override
    public void downLoadFile(String src, String fileName) {
        //SEND GET REQUEST TO "/file"  at ANOTHER PEER
        //https://gist.github.com/rponte/09ddc1aa7b9918b52029

        String requestURL = src + "?filename=" + fileName; // http://host:port/file?filename={fileName}

        File downloadedFile = new File(config.getSharedDirPath() + File.separator + fileName);

        long start = System.currentTimeMillis();

        try {
            Request.Get(requestURL).execute().handleResponse(response -> {
                final StatusLine statusLine = response.getStatusLine();
                if (statusLine.getStatusCode() != HttpServletResponse.SC_OK) {
                    throw new HttpResponseException(statusLine.getStatusCode(),
                            statusLine.getReasonPhrase());
                }
                final FileOutputStream out = new FileOutputStream(downloadedFile);
                long contentLength = 0L;
                try {

                    final HttpEntity entity = response.getEntity();
                    contentLength = entity.getContentLength();
                    if (entity != null) {
                        entity.writeTo(out);
                    }
                } finally {
                    out.close();
                }
                System.out.println(String.format("%s bytes downloaded as file %s in %s milliseconds",
                        contentLength, fileName, System.currentTimeMillis() - start));
                return null;
            });
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public File writeFileToResponse(String fileName, HttpServletResponse resp, ServletContext sc) throws FileNotFoundException {
        //http://www.codejava.net/java-ee/servlet/java-servlet-download-file-example

        //GET THE FILE OBJECT, open an inputstream against the file, then write every byte of the file into the outputstream on the response.
        //remember you need to set the response header to indicate you are providing byte-based stream rather than normal text.
        StringBuffer sb = new StringBuffer();
        sb.append(config.getSharedDirPath()).append(File.separator).append(fileName);
        File downloadFile = new File(sb.toString());
        System.out.println("" + downloadFile.getPath());
        System.out.println(downloadFile.exists());
        FileInputStream inStream = new FileInputStream(downloadFile);

        // gets MIME type of the file
        String mimeType = sc.getMimeType(fileName);
        if (mimeType == null) {
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
        }
        System.out.println("MIME type: " + mimeType);

        // modifies response
        resp.setContentType(mimeType);
        resp.setContentLength((int) downloadFile.length());
        resp.setStatus(HttpServletResponse.SC_OK);

        // forces download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        resp.setHeader(headerKey, headerValue);

        // obtains response's output stream
        OutputStream outStream;
        try {
            outStream = resp.getOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inStream.close();
            outStream.close();

        } catch (IOException ex) {
            Logger.getLogger(PeerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return downloadFile;
    }

    @Override
    public boolean isFileLocallyStored(String fileName) {
        String filePath = config.getSharedDirPath() + File.separator + fileName;
        System.out.println(filePath);
        return new File(filePath).exists();
    }
}

