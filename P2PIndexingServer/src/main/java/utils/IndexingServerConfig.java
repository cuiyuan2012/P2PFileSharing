package utils;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by cuiyuan on 3/18/16.
 */
public class IndexingServerConfig {

    private int portNum = 8080;

    private String ip_addr = "127.0.0.1";

    private Map<String, Set<String>> index = new ConcurrentHashMap<>();

    private static IndexingServerConfig instance = new IndexingServerConfig();

    public static IndexingServerConfig getInstance(){return instance;}
    private IndexingServerConfig(){}

    public Map<String, Set<String>> getIndex() {
        return index;
    }

    public String getIp_addr() {
        return ip_addr;
    }

    public void setIp_addr(String ip_addr) {
        this.ip_addr = ip_addr;
    }

    public int getPortNum() {
        return portNum;
    }

    public void setPortNum(int portNum) {
        this.portNum = portNum;
    }
}
