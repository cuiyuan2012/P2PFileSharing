package p2p;

import java.util.List;

/**
 *
 * @author cuiyuan
 */
public interface IndexingServer {
    
    public void registry(String peerId, List<String> fileList);
    
    public String search(String fileName);
}
