/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2p;

import utils.IndexingServerConfig;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author cuiyuan
 */
public class IndexingServerImpl implements IndexingServer{
    //FileName --- list of peer IDs

    private IndexingServerConfig config = IndexingServerConfig.getInstance();
    private static IndexingServerImpl instance = new IndexingServerImpl();

    private AtomicInteger rrCounter = new AtomicInteger(0);

    private IndexingServerImpl(){}

    public static IndexingServerImpl getInstance() {
        return instance;
    }
    
    Random random = new SecureRandom();
    @Override
    public void registry(String peerId, List<String> fileList) {
        for (String fileName : fileList) {
            if (fileName == null || fileName.length() == 0) {
                continue;
            }
            Set<String> peerIDs = config.getIndex().putIfAbsent(fileName, new TreeSet<>());
            if (peerIDs == null) {
                peerIDs = config.getIndex().get(fileName);
            }
            peerIDs.add(peerId);
            System.out.println(String.format("Mapping added: %s -> %s", fileName, peerId));
        }
    }

    @Override
    public String search(String fileName) {
        Set<String> peerSet = config.getIndex().get(fileName);
        if (peerSet != null) {
            String[] peerNames = peerSet.toArray(new String[peerSet.size()]);

            int indexBase = rrCounter.incrementAndGet() + random.nextInt(peerNames.length);

            System.out.println(fileName +" exists at " + Arrays.deepToString(peerNames));
            int randomIndex = indexBase % peerNames.length;//random.nextInt(peerNames.length);
            randomIndex = randomIndex> peerNames.length-1 ? 0:randomIndex;
            return peerNames[randomIndex];
        }
        return null;
    }
    
}
