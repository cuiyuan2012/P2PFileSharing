package servlet;

import p2p.IndexingServer;
import p2p.IndexingServerImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cuiyuan on 3/14/16.
 */
@WebServlet(name = "indexing", urlPatterns = {"/register", "/search"})
public class IndexingServlet extends HttpServlet {

    private IndexingServer indexingServer = IndexingServerImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        System.out.println(req.getRequestURI());
        if (req.getRequestURI().equals("/search")) {
            // query from the index. Get the peer addr
            String fileName = req.getParameter("filename");
            long start = System.currentTimeMillis();
            System.out.println("Searching file named " + fileName + "...");
            String peerAddr = indexingServer.search(fileName);
            System.out.println(String.format("Got it at %s in %s milliseconds", peerAddr, System.currentTimeMillis() - start));
            // write the peer addr into the response
            if (peerAddr != null ){
                resp.getWriter().print(peerAddr);
            } else {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "File Not Found");
            }
            return;
        }
        resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Request!");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        System.out.println(req.getRequestURI());

        if (req.getRequestURI().equals("/register")) {
            //get file name list separated by "|"
            String fileNameStr = req.getParameter("files");
            //split them into list by delimiter "|"
            List<String> fileNameList = Arrays.asList(fileNameStr.split("\\|"));
            //get peer addr from request:
            String peerAddr = req.getParameter("peer_addr");
            //save the mapping of peer_addr and filelist to the indexing
            indexingServer.registry(peerAddr, fileNameList);
            System.out.println(String.format("Peer %s registered with the file(s) below:", peerAddr));
            fileNameList.forEach(i -> System.out.println(i));

            resp.getWriter().print("File registered OK!");
            resp.getWriter().flush();

            return;
        }
        resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Request!");
    }


}
